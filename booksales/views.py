from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse

from .models import Book

def index(request):
    # List latest 5 books
    book_list = Book.objects.order_by('-initially_published')[:5]
    context = { 'book_list': book_list }
    return render(request, 'books/index.html', context)

def book_detail(request, book_id):
    book = get_object_or_404(Book, pk=book_id)
    return render(request, 'books/book_detail.html', {'book': book})

def add_bod_sales(request, book_id):
    book = get_object_or_404(Book, pk=book_id)
    
    try:
        # Get filename from request
        filename = request.POST['bod_sales_filename']
    except(FileError):
        # Redisplay the book detail form incl. the upload button.
        msg = "File " + filename + " not found."
        return render(request, 'booksales/book_detail.html', {
            'book': book,
            'error_message': msg,
        })
    else:
        # TODO: read the file
        # CSV columns: Start Date,End Date,Publisher,Title ID,ISBN,Title,Primary Author,Distributor,Vendor,Country,Units Sold,Units Returned,Net Unit Sales,List Price Per Unit,Currency Code,Offer Price Per Unit,Royalty Percent,Sales Channel Fee/Taxes Per Unit,Sales Channel Revenue Per Unit,Extended Sales Channel Revenue,Sales Channel Share,D2D Share,Publisher Share,Publisher Share USD (estimated)
        
        # TODO: create sales data objects, save them
        # for lines in file, starting at line 2
        booksale = Booksale(book = book)
        # endfor
    
        # Return a redirect to prevent POST data from being processed twice.
        return HttpResponseRedirect(reverse('booksales:book_detail', args=(book.id,)))
