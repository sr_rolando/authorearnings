from django.urls import path

from . import views

app_name = 'booksales'
urlpatterns = [
    # /booksales/
    path('', views.index, name='index'),
    # /booksales/book/5/
    path('book/<int:book_id>/', views.book_detail, name='book_detail'),
    # /booksales/add_bod_sales/
    path('book/<int:book_id>/add_bod_sales/', views.add_bod_sales, name='add_bod_sales'),
]
