from django.db import models

class Book(models.Model):
    # Mandatory. Each book better has some title.
    title = models.CharField(max_length=200, blank=False, null=False)
    
    # Optional, even though this should usually be filled. Examples: ePub, Paperback, Kindle
    format = models.CharField(max_length=30, blank=True, null=True)
    
    # Optional.
    initially_published = models.DateField('date of publishing the first edition', blank=True, null=True)
    
    # Optional. However, it really helps to put a value here.
    wordcount = models.IntegerField('Number of words in the most recent edition', blank=True, null=True)

    # Helper function to print out more helpfull info than just the ID
    def __str__(self):
        return self.title

# i.e. "BOD" or "Draft2Digital"
class Distributor(models.Model):
    name = models.CharField(max_length=200)

# i.e. "Amazon"
class ShopGroup(models.Model):
    name = models.CharField(max_length=200)

# i.e. "Amazon DE"
class Shop(models.Model):
    # The shop group (i.e. "Amazon") is totally optional.
    group = models.ForeignKey(ShopGroup, on_delete=models.SET_NULL, blank=True, null=True, verbose_name='shop group')
    
    # The distributor (i.e. "BOD" or "Draft2Digital") is totally optional.
    distributor = models.ForeignKey(Distributor, on_delete=models.SET_NULL, blank=True, null=True)
    
    name = models.CharField(max_length=200)
    url = models.CharField(max_length=256, blank=True, null=True)

class Payment(models.Model):
    date = models.DateField('date when the payment was made', blank=True, null=True)
    reference = models.CharField('Payment no as given by the shop / distributor', max_length=50, blank=True, null=True)
    amount = models.DecimalField('Actual amount as found on the bank statement', max_digits=10, decimal_places=2, blank=True, null=True)

# Country where a sale was made. See https://en.wikipedia.org/wiki/ISO_3166-1 for possible values.
class Country(models.Model):
    # This code works as a unique ID.
    code = models.CharField('ISO 3166-1 code', max_length=2, primary_key=True, blank=False, null=False)
    name = models.CharField(max_length=100, blank=True, null=True)

class Booksale(models.Model):
    # Which book was sold here?
    book = models.ForeignKey(Book, on_delete=models.CASCADE, blank=False, null=False)
    
    # This is required as each sale needs to happen at some shop.
    shop = models.ForeignKey(Shop, on_delete=models.CASCADE, blank=False, null=False)

    # Where was the sale made?
    country = models.ForeignKey(Country, on_delete=models.SET_NULL, blank=True, null=True)

    # Mandatory as this is one of the core data of this app.
    date = models.DateField('date of sale', blank=False, null=False)
    
    price = models.DecimalField('Price of sale excluding any VAT', max_digits=10, decimal_places=2, blank=False, null=False)
    currency = models.CharField('Currency code according to ISO 4217', max_length=3, blank=False, null=False)
    exchange_rate = models.DecimalField('Factor used to convert the price to EUR', max_digits=10, decimal_places=5, default=1.0)
    vat_percent = models.IntegerField('Percentage value of the VAT', default=0)
    vat_amount = models.DecimalField('Actual amount of applied VAT, in original currency, added on top of price', max_digits=10, decimal_places=2, default=0.00)
    quantity = models.DecimalField('Number of units sold / pages read', max_digits=10, decimal_places=3, default=1.0)
    earnings = models.DecimalField('Actual amount in EUR of what is payed out', max_digits=10, decimal_places=2, blank=False, null=False)
    
    # Optional as this info is likely added further down the road.
    payment = models.ForeignKey(Payment, on_delete=models.SET_NULL, blank=True, null=True)
