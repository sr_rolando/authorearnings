This is just some little app to support tracking book sales and some supportive data. This is mainly aimed at selfpublishers who want to track their earnings and see how they do over time.

By no means is this project related in any way to the awesome [AuthorEarnings](http://authorearnings.com/) project that provides excellent information and lobbying activities for authors. (But do take a look at that site. It really *is* great.)

# How to set this up

Make sure you have [Python](https://www.python.org/) installed. Then, prepare the environment:

```shell
source ~/Virtualenvs/authorearnings/bin/activate
```

Install required packages, mainly [Django](https://www.djangoproject.com/) related:

```shell
pip install django psycopg2
```

Prepare a database. [PostgreSQL](https://www.postgresql.org/) is recommended. Initialise the database:

```shell
python3 manage.py migrate
```

Create an admin user:

```shell
python manage.py createsuperuser
```

Start the development server:

```shell
python3 manage.py runserver
```

Open [http://localhost:8000/admin/](http://localhost:8000/admin/)

# Roadmap

1. Make it usable at all
2. Add some fancy [dash](https://dash.plot.ly/)board app (not decided on which one to use, yet)
